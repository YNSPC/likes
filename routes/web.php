<?php

use App\Post;
use Illuminate\Support\Facades\Route;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

Route::get('/', function () {
    $result = Post::with([
        'likedBy' => function(BelongsToMany $query){
            $query->select('users.id', 'name', 'status');
            //->wherePivot('status', 1);
            }
        ])
        ->select('posts.id', 'title')
        ->get()
        ->toJson();

    dump($result);
    return view('welcome');
});
