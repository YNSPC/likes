<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [ 'title', 'body', 'created_by' ];

    protected $hidden = ['pivot'];

    public function createdBy()
    {
        return $this->belongsTo(User::class);
    }

    public function likedBy()
    {
        return $this->belongsToMany(User::class, 'users_like_on_posts', 'post_id', 'user_id');
    }
}
