<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $table = 'users_like_on_posts';

    protected $fillable = ['user_id', 'post_id', 'status'];
}
